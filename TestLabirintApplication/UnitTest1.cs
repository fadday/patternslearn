﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using LabirintApplication;

namespace TestLabirintApplication
{
    [TestClass]
    public class TestLabirint 
    {
        [TestMethod]
        public void TestRoomConstructor()
        {
            LabirintApplication.Room testRoom = new LabirintApplication.Room(1);

            Assert.AreEqual(1, testRoom.RoomNumber, "Room number is not correct");
        }

        [TestMethod]
        public void TestRoomSetRoom()
        {
            LabirintApplication.Room testRoom = new Room(1);

            testRoom.SetSide(Direction.North, new Wall());
            testRoom.SetSide(Direction.West, new Door(new Room(1), new Room(2)));

            Assert.IsTrue(testRoom.GetSide(Direction.North).GetType() == typeof(Wall));
            Assert.IsTrue(testRoom.GetSide(Direction.West).GetType() == typeof(Door));
        }

        [TestMethod]
        public void TestDoor()
        {
            Room room1 = new Room(24);
            Room room2 = new Room(42);

            Door testDoor = new Door(room1, room2);

            Assert.AreEqual(room2, testDoor.OtherSideFrom(room1), "Other side room2 error");
            Assert.AreEqual(room1, testDoor.OtherSideFrom(room2), "Other side room1 error");
        }
        
        [TestMethod]
        public void TestMaze()
        {
            Maze testMaze = new Maze();
            Room room = new Room(42);

            testMaze.AddRoom(room);

            Assert.AreEqual(room, testMaze.RoomNo(42), "return wrong room or don't return room");
        }
    }
}
