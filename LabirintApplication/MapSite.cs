﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LabirintApplication
{

    public enum Direction
    {
        North = 0, South = 1, East = 2, West = 3
    }

    public interface IMapSite
    {
        void Enter();
    }

    public class Room : IMapSite
    {
        private int _roomNumber;
        private IMapSite[] mapSites;
        public int RoomNumber
        {
            get { return _roomNumber; }
        }
        public Room(int roomNo)
        {
            _roomNumber = roomNo;
            mapSites = new IMapSite[4];
        }
        public void Enter()
        {
            throw new NotImplementedException();
        }
        public void SetSide(Direction direction, IMapSite mapSite)
        {
            mapSites[(int)direction] = mapSite;
        }
        public IMapSite GetSide(Direction direction)
        {
            return mapSites[(int)direction];
        }
    }

    public class Wall : IMapSite
    {
        public void Enter()
        {
            throw new NotImplementedException();
        }
    }

    public class Door : IMapSite
    {
        Room _room1;
        Room _room2;

        private bool _isOpen;
        public Door(Room room1, Room room2)
        {
            _room1 = room1;
            _room2 = room2;
        }
        public void Enter()
        {
            throw new NotImplementedException();
        }

        public Room OtherSideFrom(Room room)
        {
            if (room == _room1)
                return _room2;

            if (room == _room2)
                return _room1;

            throw new ArgumentException("Door is not in this room");
        }
    }
}
