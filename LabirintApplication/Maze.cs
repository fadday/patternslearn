﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LabirintApplication
{
    public class Maze
    {
        Dictionary<int, Room> _rooms;
        public Maze()
        {
            _rooms = new Dictionary<int, Room>();
        }
        public void AddRoom(Room room)
        {
            _rooms.Add(room.RoomNumber, room);
        }
        public Room RoomNo(int roomNo)
        {
            return _rooms[roomNo];
        }
    }
}
